/*给vue实例添加原型*/
Vue.prototype.$http = axios;
Vue.prototype.baseUrl = "http://115.159.217.249:8888";
/*给所有的axios请求，添加请求前缀*/
axios.defaults.baseURL="http://localhost";
/*axios前置拦截器*/
axios.interceptors.request.use(config => {
    //携带token
    let token = localStorage.getItem("token");
    if (token) {
        //在头信息中添加token
        config.headers['token'] = token;
    }
    return config;
}, error => {
    Promise.reject(error);
});
//axios后置拦截器
axios.interceptors.response.use(result => {
    let {success, msg} = result.data;
    if(!success && msg === "noLogin"){
        //清空本地存储
        localStorage.removeItem("token");
        localStorage.removeItem("loginUser");
        location.href = "/login.html";
    }
    return result;
}, error => {
    Promise.reject(error);
});
//动态获取url地址?后面的参数
function getParam() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}