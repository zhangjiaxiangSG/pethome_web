import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Table from './views/nav1/Table.vue'
import Form from './views/nav1/Form.vue'
import user from './views/nav1/user.vue'
import Page4 from './views/nav2/Page4.vue'
import Page5 from './views/nav2/Page5.vue'
import Page6 from './views/nav3/Page6.vue'
import echarts from './views/charts/echarts.vue'
import Department from './views/org/department.vue'
import DictionaryType from './views/org/DictionaryType.vue'
import Shop from './views/ShopRegister.vue'
import Product from './views/product/Product.vue'
import pendingMessage from './views/pet/PendingMessage.vue'
import pet from './views/pet/pet.vue'
let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/shop',
        component: Shop,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '组织机构',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/department', component: Department, name: '部门管理'},
            { path: '/DictionaryType', component: DictionaryType, name: '数据字典'},
            { path: '/main', component: Main, name: '主页', hidden: true },
            { path: '/table', component: Table, name: 'Table' },
            { path: '/form', component: Form, name: 'Form' },
            { path: '/user', component: user, name: '列表' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '宠物模块',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/pendingMessage', component: pendingMessage, name: '待处理寻主信息'},
            {path: '/department1', component: Department, name: '已处理寻主信息'},
            { path: '/pet', component: pet, name: '宠物管理'},
            {path: '/department', component: Department, name: '宠物类型'},
        ]
    },
    {
        path: '/',
        component: Home,
        name: '产品管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/product', component: Product, name: '服务管理'},
        ]
    },
    {
        path: '/',
        component: Home,
        name: '导航二',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/page4', component: Page4, name: '页面4' },
            { path: '/page5', component: Page5, name: '页面5' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-address-card',
        leaf: true,//只有一个节点
        children: [
            { path: '/page6', component: Page6, name: '导航三' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: 'Charts',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/echarts', component: echarts, name: 'echarts' }
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;