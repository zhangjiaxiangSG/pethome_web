import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
//老版本的elementui
// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-default/index.css'
//import './assets/theme/theme-green/index.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
/*引入axios*/
import axios from 'axios'
Vue.prototype.$http = axios
//axios.defaults.baseURL='/api' 如果是配置的proxytable就用这个，后端跨域用下面这个
axios.defaults.baseURL='http://localhost'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'
// import Mock from './mock'
// Mock.bootstrap();
import 'font-awesome/css/font-awesome.min.css'
Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//NProgress.configure({ showSpinner: false });

const router = new VueRouter({
  routes
})
/*axios前置拦截器*/
axios.interceptors.request.use(config => {
    //携带token
    let token = localStorage.getItem("token");
    //每次请求后台的时候都会添加一个请求头，把自定义的session传给后台
    if (token) {
        //在头信息中添加token
        config.headers['token'] = token;
    }
    return config;
}, error => {
    Promise.reject(error);
});
//axios后置拦截器
axios.interceptors.response.use(result => {
    let {success, msg} = result.data;
    if(!success && msg === "noLogin"){
        //清空本地存储
        localStorage.removeItem("token");
        localStorage.removeItem("loginUser");
        router.push({ path: '/login' });
    }
    return result;
}, error => {
    Promise.reject(error);
});
//路由的拦截器
router.beforeEach((to, from, next) => {
    //如果你访问的目标资源是/login 或者/Shop 我就直接放行
    if(to.path=="/login"||to.path=="/Shop"){
        //放行让其执行目标资源
        next();
    }else{
        //如果是访问其他资源，我就要获取token是否有值
        var token = localStorage.getItem("token");
        //如果token有值，就直接放行
        if(token){
            next();
        }else{
            //那就直接跳转到登录界面
            next({path: '/login'});
        }
    }
})
// router.beforeEach((to, from, next) => {
//   //next()
//   //NProgress.start();
//   // if (to.path == '/login') {
//   //   sessionStorage.removeItem('user');
//   // }
//   // let user = JSON.parse(sessionStorage.getItem('user'));
//   // if (!user && to.path != '/login') {
//   //   next({ path: '/login' })
//   // } else {
//   //   next()
//   // }
// })

//router.afterEach(transition => {
//NProgress.done();
//});

new Vue({
  //el: '#app',
  //template: '<App/>',
  router,
  store,
  //components: { App }
  render: h => h(App)
}).$mount('#app')

